#!/bin/bash

cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI/oat/arm64/SystemUI.odex.* 2>/dev/null >> system_ext/priv-app/SystemUI/oat/arm64/SystemUI.odex
rm -f system_ext/priv-app/SystemUI/oat/arm64/SystemUI.odex.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat my_stock/app/Games/Games.apk.* 2>/dev/null >> my_stock/app/Games/Games.apk
rm -f my_stock/app/Games/Games.apk.* 2>/dev/null
cat my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null >> my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk
rm -f my_stock/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null
cat my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null >> my_stock/del-app/OPBreathMode/OPBreathMode.apk
rm -f my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null
cat my_bigball/app/Meet/Meet.apk.* 2>/dev/null >> my_bigball/app/Meet/Meet.apk
rm -f my_bigball/app/Meet/Meet.apk.* 2>/dev/null
cat my_bigball/app/Photos/Photos.apk.* 2>/dev/null >> my_bigball/app/Photos/Photos.apk
rm -f my_bigball/app/Photos/Photos.apk.* 2>/dev/null
cat my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat my_bigball/del-app-pre/Google_Home/Google_Home.apk.* 2>/dev/null >> my_bigball/del-app-pre/Google_Home/Google_Home.apk
rm -f my_bigball/del-app-pre/Google_Home/Google_Home.apk.* 2>/dev/null
cat my_bigball/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null >> my_bigball/priv-app/OppoGallery2/OppoGallery2.apk
rm -f my_bigball/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null
cat my_bigball/priv-app/Messages/Messages.apk.* 2>/dev/null >> my_bigball/priv-app/Messages/Messages.apk
rm -f my_bigball/priv-app/Messages/Messages.apk.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat my_heytap/app/StdSP/StdSP.apk.* 2>/dev/null >> my_heytap/app/StdSP/StdSP.apk
rm -f my_heytap/app/StdSP/StdSP.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null >> my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk
rm -f my_heytap/app/SpeechServicesByGoogle/SpeechServicesByGoogle.apk.* 2>/dev/null
cat my_heytap/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null >> my_heytap/priv-app/Phonesky/Phonesky.apk
rm -f my_heytap/priv-app/Phonesky/Phonesky.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat my_product/app/OPLiveWallpaper/OPLiveWallpaper.apk.* 2>/dev/null >> my_product/app/OPLiveWallpaper/OPLiveWallpaper.apk
rm -f my_product/app/OPLiveWallpaper/OPLiveWallpaper.apk.* 2>/dev/null
cat my_product/priv-app/OnePlusCamera/OnePlusCamera.apk.* 2>/dev/null >> my_product/priv-app/OnePlusCamera/OnePlusCamera.apk
rm -f my_product/priv-app/OnePlusCamera/OnePlusCamera.apk.* 2>/dev/null
