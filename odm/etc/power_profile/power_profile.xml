<?xml version="1.0" encoding="utf-8"?>
<!--
**
** Copyright 2017, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License")
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->
<device name="Android">
    <!-- All values are in mAh except as noted.
         This file is for PowerProfileTest.java. Changes must be synced between these two. Since
         power_profile.xml may be overridden by actual device's power_profile.xml at compile time,
         this test config ensures we have something constant to test against. Values below are
         sample values, not meant to reflect any real device.
    -->

    <!-- This is the battery capacity in mAh -->
    <item name="battery.capacity">4500</item>

    <!-- Number of cores each CPU cluster contains -->
    <array name="cpu.clusters.cores">
        <value>6</value> <!-- Cluster 0 has 6 cores (cpu0, cpu1, cpu2, cpu3,cpu4, cpu5) -->
        <value>2</value> <!-- Cluster 1 has 2 cores ( cpu6, cpu7) -->
    </array>

    <!-- Power consumption when CPU is suspended -->
    <item name="cpu.suspend">5.3</item>
    <!-- Additional power consumption when CPU is in a kernel idle loop -->
    <item name="cpu.idle">1.11</item>
    <!-- Additional power consumption by CPU excluding cluster and core when  running -->
    <item name="cpu.active">2.55</item>

    <!-- Additional power consumption by CPU cluster0 itself when running excluding cores in it -->
    <item name="cpu.cluster_power.cluster0">2.11</item>
    <!-- Additional power consumption by CPU cluster1 itself when running excluding cores in it -->
    <item name="cpu.cluster_power.cluster1">2.22</item>

    <!-- Different CPU speeds as reported in
         /sys/devices/system/cpu/cpu0/cpufreq/stats/scaling_available_frequencies -->
    <array name="cpu.core_speeds.cluster0">
        <value>300000</value>
        <value>576000</value>
        <value>768000</value>
        <value>1017600</value>
        <value>1248000</value>
        <value>1324800</value>
        <value>1516800</value>
        <value>1612800</value>
        <value>1708800</value>
        <value>1804800</value>
    </array>
    <!-- Different CPU speeds as reported in
         /sys/devices/system/cpu/cpu4/cpufreq/stats/scaling_available_frequencies -->
    <array name="cpu.core_speeds.cluster1">
        <value>300000</value>
        <value>787200</value>
        <value>979200</value>
        <value>1036800</value>
        <value>1248000</value>
        <value>1401600</value>
        <value>1555200</value>
        <value>1766400</value>
        <value>1900800</value>
        <value>2073600</value>
        <value>2131200</value>
        <value>2208000</value>
    </array>

    <!-- Additional power used by a CPU from cluster 0 when running at different
         speeds. Currently this measurement also includes cluster cost. -->
    <array name="cpu.core_power.cluster0">
        <value>34.5</value> <!-- 300 MHz CPU speed -->
        <value>40</value> <!-- 600 MHz CPU speed -->
        <value>43.7</value> <!-- 768 MHz CPU speed -->
        <value>51</value> <!-- 1000 MHz CPU speed -->
        <value>59</value> <!-- 1248 MHz CPU speed -->
        <value>66.6</value> <!-- 1324 MHz CPU speed -->
        <value>72</value> <!-- 1516 MHz CPU speed -->
        <value>77.8</value> <!-- 1612 MHz CPU speed -->
        <value>83</value> <!-- 1708 MHz CPU speed -->
        <value>90.2</value> <!-- 1804 MHz CPU speed -->
    </array>
    <!-- Additional power used by a CPU from cluster 1 when running at different
         speeds. Currently this measurement also includes cluster cost. -->
    <array name="cpu.core_power.cluster1">
        <value>21</value> <!-- 300 MHz CPU speed -->
        <value>32</value> <!-- 787 MHz CPU speed -->
        <value>70</value> <!-- 979 MHz CPU speed -->
        <value>78</value> <!-- 1036 MHz CPU speed -->
        <value>90</value> <!-- 1248 MHz CPU speed -->
        <value>108</value> <!-- 1401 MHz CPU speed -->
        <value>113</value> <!-- 1555 MHz CPU speed -->
        <value>120</value> <!-- 1766 MHz CPU speed -->
        <value>132</value> <!-- 1900 MHz CPU speed -->
        <value>144</value> <!-- 2073 MHz CPU speed -->
        <value>155</value> <!-- 2131 MHz CPU speed -->
        <value>168</value> <!-- 2208 MHz CPU speed -->
    </array>

    <!-- Power used by display unit in ambient display mode, including back lighting-->
    <item name="ambient.on">32</item>
    <!-- Additional power used when screen is turned on at minimum brightness -->
    <item name="screen.on">88.2</item>
    <!-- Additional power used when screen is at maximum brightness, compared to
         screen at minimum brightness -->
    <item name="screen.full">373.71</item>

    <!-- Average power used by the camera flash module when on -->
    <item name="camera.flashlight">72</item>
    <!-- Average power use by the camera subsystem for a typical camera
         application. Intended as a rough estimate for an application running a
         preview and capturing approximately 10 full-resolution pictures per
         minute. -->
    <item name="camera.avg">620</item>

    <!-- Additional power used by the audio hardware, probably due to DSP -->
    <item name="audio">34.66</item>
    <item name="bluetooth.active">4.8</item> <!-- Bluetooth data transfer, ~10mA -->
    <item name="bluetooth.on">0.81</item>  <!-- Bluetooth on & connectable, but not connected, ~0.1mA -->
    <item name="wifi.on">0.18</item>  <!-- ~3mA -->
    <item name="wifi.active">165</item>  <!-- WIFI data transfer, ~200mA -->
    <!-- Additional power used by the video hardware, probably due to DSP -->
    <item name="video">70</item> <!-- ~50mA -->

    <!-- Additional power used when GPS is acquiring a signal -->
    <item name="gps.on">56</item>

    <!-- Additional power used when cellular radio is transmitting/receiving -->
    <item name="radio.active">146</item>
    <!-- Additional power used when cellular radio is paging the tower -->
    <item name="radio.scanning">1.8</item>
    <!-- Additional power used when the cellular radio is on. Multi-value entry,
         one per signal strength (no signal, weak, moderate, strong) -->
    <array name="radio.on"> <!-- Strength 0 to BINS-1 -->
        <value>6</value>       <!-- none -->
        <value>5</value>       <!-- poor -->
        <value>3</value>       <!-- moderate -->
        <value>1.6</value>       <!-- good -->
        <value>1.6</value>       <!-- great -->
    </array>
    <!-- GmsCore must always have network access for GCM and other things. -->
    <allow-in-power-save package="com.google.android.gms" />
    <allow-in-power-save package="com.oppo.market" />
    <allow-in-data-usage-save package="com.google.android.gms" />
</device>
