{
    "Image_Build_IDs": {
        "adsp": "ADSP.VT.5.6-00624.1-BITRA-1", 
        "aop": "AOP.HO.2.0-00067-BITRAAAAAANAZO-1", 
        "boot": "BOOT.XF.3.3-00307-BITRALAZ-1", 
        "btfm": "BTFM.CHE.3.2.1-00210-QCACHROMZ-1", 
        "btfm_CHK": "BTFM.CHE.2.1.5-00263-QCACHROMZ-1", 
        "cdsp": "CDSP.VT.2.6-00521-BITRA-1.378131.2", 
        "common": "Bitra.LA.2.0.c1-00002-STD.PROD-1.378131.1", 
        "glue": "GLUE.BITRA.LA.1.0-00158-NOOP_TEST-1", 
        "modem": "MPSS.HI.2.0.1.c6-00213-BITRA_GEN_PACK-1", 
        "npu": "NPU.FW.2.3-00057-BITRA_NPU_PACK-1", 
        "tz": "TZ.XF.5.10-00233-BITRAAAAAANAZT-1", 
        "tz_apps": "TZ.APPS.2.0-00131-BITRAAAAAANAZT-4", 
        "video": "VIDEO.VPU.1.2-00036-PROD-1", 
        "wlan": "WLAN.HL.3.3.1-00762-QCAHLSWMTPLZ-1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "Bitra.LA.2.0.c1-00002-STD.PROD-1.378131.1", 
        "Product_Flavor": "asic", 
        "Time_Stamp": "2023-02-13 00:37:21"
    }, 
    "Version": "1.0"
}